from flask import Flask, request, jsonify
from werkzeug.exceptions import BadRequest
import pymongo
from bson.objectid import ObjectId
from PIL import Image
import numpy as np
from scipy.linalg import svd
app = Flask(__name__)

DEFAULT_IMAGE_SIZE = 10

db = pymongo.MongoClient()['testapi']['results']

@app.route('/',methods=['GET','POST'])
def api():
    if request.method == 'POST':
        return do_post()
    else:
        return do_get()
def do_post():
    # make sure request contains a file
    if 'file' not in request.files:
        raise BadRequest('No image')
    file = request.files['file']
    # make double sure
    if file.filename == '':
        raise BadRequest('No image')
    # get parameter for image resizing
    size = request.form.get('size',DEFAULT_IMAGE_SIZE)
    if size:
        size = int(size)
    else:
        size = DEFAULT_IMAGE_SIZE
    if size < 1:
        raise BadRequest('Size must be a positive integer')
    # convert image to grayscale and save as matrix
    image = Image.open(file)
    image.thumbnail((size,size))
    image = np.asarray(image.convert('L'))
    # do singular value decomposition
    U, s, VT = svd(image)
    # compose result
    result = {
            'U':U.tolist(),
            's':s.tolist(),
            'VT':VT.tolist()
            }
    # put in database
    db.insert_one(result)
    # format for json
    result['_id'] = str(result['_id'])
    return jsonify(result)
def do_get():
    # see if id arg is present
    id = request.args.get('id', default = None)
    if id:
        # get result from database
        result = db.find_one({'_id':ObjectId(id)})
        # format for json
        result['_id'] = str(result['_id'])
        return jsonify(result)
    else:
        # simple html for testing the api
        html = '''
    <!doctype html>
    <title>Upload new Image</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <h1>Upload new Image</h1>
    <form method=post enctype=multipart/form-data>
      <p>
      <label for=size>Resize image to fit within (pixels)</label>
      <input type=number name=size id=size value='''+str(DEFAULT_IMAGE_SIZE)+'''>
      </p>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form>
    <h1>Get stored singular value decomposition</h1>
    '''
        # list of ids as links
        for id in db.distinct('_id'):
            html += '<p><a href="?id='+str(id)+'">'+str(id)+'</a></p>'
        return html
@app.errorhandler(BadRequest)
def handle_bad_request(e):
    return e, 400
