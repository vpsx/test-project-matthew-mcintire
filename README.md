# Test Project: Matthew Mcintire

## Description
Your task is to create a REST API using the Flask package with Python to create a single endpoint. David Ostby will supply you with information about what kind of processing the endpoint should do and what data will need to be sent to the endpoint.

You will need to implement the behavior for two HTTP methods: `POST` and `GET`. You will also need to store the results of the processing in MongoDB for retrieval later.

Use this git repository to make commits and push to as you progress. Remember to use good practices when making commits and using commit messages.

### Method: POST
Sending the data to this endpoint will result in new computations being executed and the results being stored in the db. You should send the data as JSON in the request's body. The response should contain the results as JSON.

### Method: GET
When sending a `GET` request the client will need to provide one query string parameter: `id`. The API will then find the results pertaining to that id and return it as JSON.
For example, if the `id` were `1`, the request URL would look like `http://myapi.io/?id=1`